//
//  ViewController.swift
//  MVC Example
//
//  Created by Himanshu Chimanji on 15/11/18.
//  Copyright © 2018 Himanshu Chimanji. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController, UITableViewDelegate , UITableViewDataSource  {

    @IBOutlet weak var exampleTableView: UITableView!
    
    
    let RANDOM_USER_URL = "https://newsapi.org/v2/everything?q=bitcoin&from=2018-12-20&sortBy=publishedAt&apiKey=26bbf72605a945dd9188202a5538a0a4"
    var rootModal = [Article]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        exampleTableView.delegate = self
        exampleTableView.dataSource = self
        callApi()
        
    }

    func callApi()
    {
        Alamofire.request(RANDOM_USER_URL).responseObject { (response: DataResponse<UserResponse>) in
            let list = response.result.value
            
            if let userData = list?.articles
            {
                for(_, user) in userData.enumerated(){
                    self.rootModal.append(user)
                }
            }
            self.exampleTableView.reloadData()

        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rootModal.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExampleTableViewCell", for: indexPath) as! ExampleTableViewCell
        cell.confiqureUserCell(item: rootModal[indexPath.row])
        return cell
    }
}

