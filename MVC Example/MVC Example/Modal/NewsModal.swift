//
//  NewsModal.swift
//  MVC Example
//
//  Created by Himanshu Chimanji on 20/12/18.
//  Copyright © 2018 Himanshu Chimanji. All rights reserved.
//

import Foundation
import ObjectMapper

class UserResponse: Mappable {
    
    var articles : [Article]?
    var status : String?

    
    
    func mapping(map: Map)
    {
        articles <- map["articles"]
        status <- map["status"]

    }

    required init?(map: Map) {
        
    }
    
}
class Article: Mappable {
    
    var author : String?
    var descriptionField : String?
    var title : String?
    var urlToImage : String?
    
    
    func mapping(map: Map)
    {
        author <- map["author"]
        descriptionField <- map["description"]
        title <- map["title"]
        urlToImage <- map["urlToImage"]
    }
    required init?(map: Map) {
        
    }
}
