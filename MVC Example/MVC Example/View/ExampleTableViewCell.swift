//
//  ExampleTableViewCell.swift
//  MVC Example
//
//  Created by Himanshu Chimanji on 20/12/18.
//  Copyright © 2018 Himanshu Chimanji. All rights reserved.
//

import UIKit
import SDWebImage

class ExampleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func confiqureUserCell(item: Article){
        nameLbl.text = item.author
        titleLbl.text = item.title
        descriptionLbl.text = item.descriptionField
        let imageURL = URL(string: item.urlToImage ?? "NoImage")
        newsImage.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "no-image-available"))
    }


}
