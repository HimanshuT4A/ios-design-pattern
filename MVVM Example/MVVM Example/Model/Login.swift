//
//  Login.swift
//  MVVM Example
//
//  Created by Himanshu Chimanji on 15/11/18.
//  Copyright © 2018 Himanshu Chimanji. All rights reserved.
//

import Foundation
import ObjectMapper

class Login: Mappable {
    
    var userName: String?
    var password: String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        userName <- map["username"]
        password <- map["password"]
        
    }
    
    
}
