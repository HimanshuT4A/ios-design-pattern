//
//  UserResponse.swift
//  MVVM Example
//
//  Created by Himanshu Chimanji on 15/11/18.
//  Copyright © 2018 Himanshu Chimanji. All rights reserved.
//

import Foundation
import ObjectMapper

class UserResponse: Mappable {
    
    var userModelList : [UsersModel]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        userModelList <- map ["results"]
    }
}
