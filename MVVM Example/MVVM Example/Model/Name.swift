//
//  Name.swift
//  MVVM Example
//
//  Created by Himanshu Chimanji on 15/11/18.
//  Copyright © 2018 Himanshu Chimanji. All rights reserved.
//


import Foundation
import ObjectMapper

class Name: Mappable {
    
    var title: String?
    var first: String?
    var last: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        first <- map["first"]
        last <- map["last"]
    }
    
    func getFullName()-> String {
        
        return title! + ": " + first! + " " + last!
    }
}
