//
//  UsersModel.swift
//  MVVM Example
//
//  Created by Himanshu Chimanji on 15/11/18.
//  Copyright © 2018 Himanshu Chimanji. All rights reserved.
//

import Foundation
import ObjectMapper

class UsersModel: Mappable{
    
    var gender: String?
    var email: String?
    var phone: String?
    var cell: String?
    var name: Name?
    var location: Location?
    var login: Login?
    var picture: Picture?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        gender <- map["gender"]
        email <- map["email"]
        phone <- map["phone"]
        cell <- map["cell"]
        name <- map["name"]
        location <- map["location"]
        login <- map["login"]
        picture <- map["picture"]
        
        
        
        
        
    }
    
}
