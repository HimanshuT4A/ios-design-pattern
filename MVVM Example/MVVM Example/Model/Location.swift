//
//  Location.swift
//  MVVM Example
//
//  Created by Himanshu Chimanji on 15/11/18.
//  Copyright © 2018 Himanshu Chimanji. All rights reserved.
//


import Foundation
import ObjectMapper

class Location: Mappable {
    
    var street: String?
    var city: String?
    var state: String?
    var postcode: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        street <- map["street"]
        city <- map["city"]
        state <- map["state"]
        postcode <- map["postcode"]
        
    }
    
}
