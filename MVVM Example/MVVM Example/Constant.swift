//
//  Constant.swift
//  MVVM Example
//
//  Created by Himanshu Chimanji on 15/11/18.
//  Copyright © 2018 Himanshu Chimanji. All rights reserved.
//

import Foundation
import UIKit

let RANDOM_USER_URL = "http://api.randomuser.me/?results=10&nat=e"
let GITHUB_URL = ""
let SHADOW_GRY: CGFloat = 120.0 / 255.0


typealias DownloadComplete = () -> ()
