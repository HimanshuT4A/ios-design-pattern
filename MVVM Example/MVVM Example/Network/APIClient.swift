//
//  APIClient.swift
//  MVVM Example
//
//  Created by Himanshu Chimanji on 15/11/18.
//  Copyright © 2018 Himanshu Chimanji. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


class APIClient: NSObject {
    
    var _usersList = [UsersModel] ()
    
//    var usersList: [UsersModel] {
//        return _usersList
//    }
    
    // to download users data Json from the API
    func downloadUser(complete: @escaping DownloadComplete) {
        
        Alamofire.request(RANDOM_USER_URL).responseObject{ (response: DataResponse<UserResponse>) in
            
            let list = response.result.value
            
            if let usersModel = list?.userModelList {
                
                for(_ , user) in usersModel.enumerated() {
                    
                    self._usersList.append(user)
                }
            }
            complete()
        }
    }
    
}
